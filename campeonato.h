/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */


#ifndef CAMPEONATO_H
#define CAMPEONATO_H
typedef struct championship Campeonato, *pCampeonato;
typedef struct ladder Podio, *pPodio;
typedef struct run Prova, *pProva;

struct championship{
    int total_provas;
    int provas_decorridas;
    int tam_podio;
    pProva provas;
    pPodio podio;
};

struct ladder{
    int acidente;
    int pontos;
    int corridas_participadas;
    Piloto * piloto;
    pPodio proximo;
};
struct run{
    pCorrida corrida;
    pProva proxima;
};


pCampeonato criar_campeonato(pCampeonato campeonato, int total_provas);
pPodio atualiza_podio(pCorrida corrida, pPodio podio);
void escreve_podio(pPodio podio);
void ordena_podio(pPodio podio);
int verifica_podio(pPodio a, pPodio b);
void troca_podio(pPodio a,pPodio b);
int no_podio(pPodio podio, Piloto * piloto);
int guardar_campeonato(pCampeonato campeonato, char * filename);
pCampeonato carregar_campeonato(pCampeonato campeonato, char * filename);
pCampeonato avanca_campeonato(pCampeonato campeonato,int capacidade, int distancia, int voltas ,Piloto * lista_pilotos, Carro * lista_carros,int size_pilotos,int size_carros);
int tamanho_podio(pPodio podio);
#endif /* CAMPEONATO_H */