/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilotos.h"
#include "carros.h"
#include "utils.h"
#include "corridas.h"
#include "campeonato.h"

#define FICHEIRO_PILOTOS "lista_pilotos.txt"
#define FICHEIRO_CARROS "lista_carros.txt"
#define FICHEIRO_CAMPEONATO "campeonato.dat"
/*
 * 
 */


void liberta_campeonato(pCampeonato camp);
void liberta_corrida(pCorrida corrida);
void liberta_podio(pPodio podio);
int file_exists(char * filename);
int remove_file(char * filename);
int safe_input_read();



int main(int argc, char** argv) {
    int size_pilotos, size_carros;
    Piloto *lista_pilotos = carrega_pilotos(FICHEIRO_PILOTOS,&size_pilotos);
    Carro *lista_carros = carrega_carros(FICHEIRO_CARROS,&size_carros);
    Corrida *corrida_individual = NULL;
    pCampeonato campeonato = NULL;
    int option = 0, optionA = 0, optionB = 0, camp_existe = 0;
    int valid;
    int voltas_corrida = 0;
    int distancia_corrida = 0;
    int numero_pilotos = 0;
    int volta_selecionada = 0;
    int id_piloto_penalizado = 0;
    int dias_penalizacao = 0; 
    int piloto_penalizado = 0;
    int numero_provas = 0;
    int prova_selecionada = 0;
    pProva provas;
    
    
    if(lista_pilotos == NULL || size_pilotos == 0){
        printf("[ERRO] - Nao foi possivel ler a lista de pilotos!\n");
        return EXIT_FAILURE;
    }
    if(lista_carros == NULL || size_carros == 0){
        printf("[ERRO] - Nao foi possivel ler a lista de carros!\n");
        return EXIT_FAILURE;
    }
    printf("Bem-Vindo/a aos Karts@ISEC\n");
    printf("Projeto desenvolvido por: \n Aluno: Andre Melo\n Numero: 21270968\n Ano: 2018/2019\n\n");
    if(file_exists(FICHEIRO_CAMPEONATO)){
        campeonato = carregar_campeonato(campeonato,FICHEIRO_CAMPEONATO);
        printf("total_provas: %d\n",campeonato->total_provas);
        printf("provas_decorridas: %d\n",campeonato->provas_decorridas);
        printf("voltas: %d\n",campeonato->provas->corrida->voltas);
        escreve_resultados(campeonato->provas->corrida->resultados,campeonato->provas->corrida->voltas);
        escreve_podio(campeonato->podio);
        camp_existe = 1;
    }
    
    do{
        do{
            if(camp_existe == 1){
                valid = 1;
                option = 2;
                camp_existe = 0;
                break;
            }
            valid = 0;
            option = 0;
            printf("\nMenu Inicial\n");
            if(campeonato == NULL){
                printf(" [1] - Corrida Individual/Treino \n");
            }
            printf(" [2] - Campeonato\n");
            printf(" [3] - Penalizar Piloto\n");
            printf(" [4] - Visualizar Pilotos\n");
            printf(" [5] - Visualizar Carros\n");
            printf(" [0] - Sair\n");
            printf("Selecione uma opcao: \n");
            option = safe_input_read();
            if((option > 1 && option <= 5) || option == 0){
                valid = 1;
            }
            if(option == 1 && campeonato == NULL){
                valid = 1;
            }
        }while(valid == 0);
        switch(option){
            case 1:
                do{
                    do{
                        valid = 0;
                        printf("\nMenu Corrida Individual/Treino\n");
                        printf(" [1] - Criar corrida\n");
                        if(corrida_individual != NULL && corrida_individual->resultados == NULL){
                            printf(" [2] - Iniciar corrida\n");
                        }
                        if(corrida_individual != NULL && corrida_individual->resultados != NULL){
                            printf(" [3] - Visualizar resultado de uma volta\n");
                            printf(" [4] - Visualizar resultados\n");
                        }
                        printf(" [0] - Voltar\n");
                        printf("Selecione uma opcao: \n");
                        optionA = safe_input_read();
                        if(optionA == 1 || optionA == 0){
                            valid = 1;
                        }
                        if(optionA >= 2 && optionA <= 4){
                            valid = 0;
                            if((optionA == 2) && (corrida_individual != NULL && corrida_individual->resultados == NULL)){
                                valid = 1;
                            }
                            if((optionA == 3 || optionA == 4) && corrida_individual != NULL && corrida_individual->resultados != NULL){
                                valid = 1;
                            }
                            
                        }
                    }while(valid == 0);
                    switch(optionA){
                        case 1:
                            printf("\nMenu Criar Corrida\n");
                            do{
                                printf("Indique o numero de voltas [5,10]: \n");
                                voltas_corrida = safe_input_read();
                            }while(voltas_corrida < 5 || voltas_corrida > 10);
                            do{
                                printf("Indique o cumprimento da pista [500,1000]: \n");
                                distancia_corrida = safe_input_read();
                            }while(distancia_corrida < 500 || distancia_corrida > 1000);
                            do{
                                printf("Indique o numero maximo de concorrentes [1,%d]: \n",size_pilotos);
                                numero_pilotos = safe_input_read();
                            }while(numero_pilotos < 1 || numero_pilotos > size_pilotos);
                            corrida_individual = criar_corrida(voltas_corrida,distancia_corrida,numero_pilotos,lista_pilotos,lista_carros,size_pilotos,size_carros);
                            break;
                        case 2:
                            printf("\nMenu Iniciar Corrida\n");
                            corrida_individual = inicia_corrida(corrida_individual);
                            atualiza_listas(corrida_individual,lista_pilotos,lista_carros,size_pilotos,size_carros);
                            break;
                        case 3:
                            printf("\nMenu Mostrar Resultados de Volta\n");
                            do{
                                printf("Indique o numero da volta [1,%d]: \n",voltas_corrida);
                                volta_selecionada = safe_input_read();
                            }while(volta_selecionada < 1 || volta_selecionada > voltas_corrida);
                            escreve_resultados_volta(corrida_individual->resultados,volta_selecionada);
                            break;
                        case 4:
                            printf("\nMenu Mostrar Resultados de Corrida\n");
                            escreve_resultados(corrida_individual->resultados,voltas_corrida);
                            break;
                            
                    }
                }while(optionA != 0); 
                break;
            case 2:
                do{
                    do{
                        valid = 0;
                        printf("\nMenu Campeonato\n");
                        printf(" [1] - Criar campeonato\n");
                        if(campeonato != NULL){
                            if(campeonato->provas_decorridas < campeonato->total_provas){
                                printf(" [2] - Iniciar proxima prova (%d/%d) \n",campeonato->provas_decorridas,campeonato->total_provas);
                            }
                            if(campeonato->provas_decorridas > 0){
                                printf(" [3] - Visualizar resultados do campeonato\n");
                                printf(" [4] - Visualizar resultados de uma prova\n");
                                printf(" [5] - Visualizar podio\n");
                                
                            }
                            printf(" [6] - Guardar campeonato\n");
                            if(file_exists(FICHEIRO_CAMPEONATO)){
                                printf(" [7] - Apagar campeonato\n");
                            }
                        }
                        printf(" [0] - Voltar\n");
                        printf("Selecione uma opcao: \n");
                        optionB = safe_input_read();
                        if(campeonato != NULL){
                            if(optionB == 1){
                                valid = 1;
                            }
                            if(optionB == 2 && (campeonato->provas_decorridas < campeonato->total_provas)){
                                valid = 1;
                            }
                            if((optionB >= 3 && optionB <= 5 ) && (campeonato->provas_decorridas > 0)){
                                valid = 1;
                            }
                            if(optionB == 6 || optionB == 0 || (optionB == 7 && file_exists(FICHEIRO_CAMPEONATO))){
                                valid = 1;
                            }
                        }else{
                            if(optionB == 1 || optionB == 0){
                                valid = 1;
                            }
                        }
                        }while(valid == 0);
                        switch(optionB){
                            case 1:
                                printf("\nMenu Criar Campeonato\n");
                                do{
                                    printf("Indique o numero de provas [3,8]: \n");
                                    numero_provas = safe_input_read();
                                }while(numero_provas < 3 || numero_provas > 8);
                                campeonato = criar_campeonato(campeonato,numero_provas);
                            break;
                            case 2:
                                printf("\nMenu Iniciar Prova\n");
                                do{
                                    printf("Indique o numero de voltas [5,10]: \n");
                                    voltas_corrida = safe_input_read();
                                }while(voltas_corrida < 5 || voltas_corrida > 10);
                                do{
                                    printf("Indique o cumprimento da pista [500,1000]: \n");
                                    distancia_corrida = safe_input_read();
                                }while(distancia_corrida < 500 || distancia_corrida > 1000);
                                do{
                                    printf("Indique o numero maximo de concorrentes [1,%d]: \n",size_pilotos);
                                    numero_pilotos = safe_input_read();
                                }while(numero_pilotos < 1 || numero_pilotos > size_pilotos);
                                avanca_campeonato(campeonato,numero_pilotos,distancia_corrida,voltas_corrida,lista_pilotos,lista_carros,size_pilotos,size_carros);
                                break;
                            case 3:
                                printf("\nMenu Mostrar Resultados\n");
                                provas = campeonato->provas;
                                for(int p = 1; p <=campeonato->provas_decorridas; p++ ){
                                    printf("\nResultados da prova #%d\n",p);
                                    escreve_resultados(provas->corrida->resultados,provas->corrida->voltas);
                                    provas = provas->proxima;
                                }
                                escreve_podio(campeonato->podio);
                                break;
                            case 4:
                                printf("\nMenu Mostrar Resultados de Prova\n");
                                do{
                                    printf("Indique o numero da prova [1,%d]: \n",campeonato->provas_decorridas);
                                    prova_selecionada = safe_input_read();
                                }while(prova_selecionada < 1 || prova_selecionada > campeonato->provas_decorridas);
                                provas = campeonato->provas;
                                for(int p = 1; p <prova_selecionada; p++ ){
                                    provas = provas->proxima;
                                }
                                escreve_resultados(provas->corrida->resultados,provas->corrida->voltas);
                                break;
                            case 5:
                                printf("\nMenu Mostrar Podio\n");
                                escreve_podio(campeonato->podio);
                                break;
                            case 6:
                                printf("\nMenu Guardar Campeonato\n");
                                if(guardar_campeonato(campeonato, FICHEIRO_CAMPEONATO)){
                                    printf("[SUCESSO] - Campeonato guardado! \n");
                                }else{
                                    printf("[ERRO] - Nao foi possivel guardar o campeonato!\n");
                                }
                                break;
                            case 7:
                                printf("\nMenu Apagar Campeonato\n");
                                if(remove_file(FICHEIRO_CAMPEONATO)){
                                    printf("[SUCESSO] - Campeonato eliminado! \n");
                                    liberta_campeonato(campeonato);
                                    campeonato = NULL;
                                }else{
                                    printf("[ERRO] - Nao foi possivel eliminar o campeonato!\n");
                                }
                                break;
                        }
                }while(optionB != 0);
                break;
            case 3:
                printf("\nMenu Penalizar Piloto\n");
                escreve_pilotos(lista_pilotos,size_pilotos);
                do{
                    printf("Indique o id do piloto: \n");
                    id_piloto_penalizado = safe_input_read();
                }while(id_piloto_penalizado < 0);
                do{
                    printf("Indique o numero de corridas de penalizacao [1,3]: \n");
                    dias_penalizacao = safe_input_read();
                }while(dias_penalizacao < 1 || dias_penalizacao > 3);
                
                piloto_penalizado = penalizar_piloto(id_piloto_penalizado, dias_penalizacao, lista_pilotos, size_pilotos);
                if( piloto_penalizado >= 0){
                    printf("O piloto %s foi penalizado com %d dias!\n",lista_pilotos[piloto_penalizado].nome,dias_penalizacao);
                }else{
                    printf("Nao foi possivel encontrar um piloto com o id : %d\n",id_piloto_penalizado);
                }
                
                break;
            case 4:
                escreve_pilotos(lista_pilotos,size_pilotos);
                break;
            case 5:
                escreve_carros(lista_carros,size_carros);
                break;
        }
    }while(option != 0);
    guarda_pilotos(FICHEIRO_PILOTOS,lista_pilotos,size_pilotos);
    guarda_carros(FICHEIRO_CARROS,lista_carros,size_carros);
    if(corrida_individual != NULL){
        liberta_corrida(corrida_individual);
    }
    if(campeonato != NULL){
        liberta_campeonato(campeonato);
    }
    if(lista_carros != NULL){
        free(lista_carros);
    }
    if(lista_pilotos != NULL){
        free(lista_pilotos);
    }

    return (EXIT_SUCCESS);
}
void liberta_campeonato(pCampeonato camp){
    pProva provas = camp->provas;
    while(provas != NULL){
        pProva aux = provas;
        provas = provas->proxima;
        liberta_corrida(aux->corrida);
        free(aux->corrida);
    }
    if(camp->podio != NULL){
        liberta_podio(camp->podio);
    }
    free(camp);
}
void liberta_corrida(pCorrida corrida){
    free(corrida->carros);
    free(corrida->carros_fora);
    free(corrida->pilotos);
    free(corrida->pilotos_fora);
    if(corrida->resultados != NULL){
        if(corrida->resultados->carro != NULL){
            free(corrida->resultados->carro);
        }
        if(corrida->resultados->piloto != NULL){
            free(corrida->resultados->piloto);
        }
        pVolta voltas = corrida->resultados->voltas;
        while(voltas != NULL){
            pVolta aux = voltas;
            voltas = voltas->proxima;
            free(aux);
        }
        free(corrida->resultados);
    }
}
void liberta_podio(pPodio podio){
    free(podio->piloto);
}
int file_exists(char * filename){
    FILE * file = fopen(filename, "r");
    if (file){
        fclose(file);
        return 1;
    }
    return 0;
}
int remove_file(char * filename){
    if(remove(filename) == 0){
        return 1;
    }
    return 0;
}
int safe_input_read(){
    int size = 1024;
    char data[size];
    char *lixo;
    fgets(data, size, stdin);
    fseek(stdin,0,SEEK_END);
    int val = strtol(data, &lixo, 10);
    if(val == 0 && data[0] != '0'){
        return -1;
    }
    return val;
}