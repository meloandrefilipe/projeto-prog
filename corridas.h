/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */

#ifndef CORRIDAS_H
#define CORRIDAS_H
typedef struct scores Resultados, *pResultado;
typedef struct laps Voltas, *pVolta;
typedef struct race Corrida, *pCorrida;

struct race{
    int voltas;
    int cumprimento;
    int capacidade;
    int pilotos_prova;
    int carros_prova;
    int pilotos_penalizados;
    int carros_penalizados;
    int tamanho;
    Carro* carros;
    Piloto* pilotos;
    Carro* carros_fora;
    Piloto* pilotos_fora;
    pResultado resultados;
};

struct laps{
    int volta;
    int tempo;
    pVolta proxima;
};

struct scores{
    Carro* carro;
    Piloto* piloto;
    pVolta voltas;
    int acidente;
    int tempo_total;
    pResultado proximo;
};



Corrida * criar_corrida(int voltas, int cumprimento, int capacidade, Piloto * lista_pilotos, Carro * lista_carros,int size_pilotos,int size_carros);
int carro_aleatorio(Corrida * corrida, int data[][2], int size);
int piloto_aleatorio(Corrida * corrida, int data[][2], int size);
Corrida * inicia_corrida(Corrida * corrida);
void ordenar_resultados(pResultado resultado, int volta);
void escreve_resultados(pResultado resultados, int voltas);
void atualizar_penalizacoes(Piloto * pilotos, Carro * carros, int size_pilotos,int size_carros );
int verifica_troca(pResultado a, pResultado b, int volta);
void troca_valores(pResultado a, pResultado b);
void escreve_resultados_volta(pResultado resultados, int volta);
Corrida * escreve_resultados_atualiza(Corrida * corrida);
void atualiza_listas(Corrida * corrida, Piloto * pilotos, Carro * carros, int size_pilotos, int size_carros);
void escreve_selecao(Corrida * corrida, int emparelhamento[][2]);
#endif /* CORRIDAS_H */

