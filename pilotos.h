/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */


#ifndef PROJETO_PILOTOS_H
#define PROJETO_PILOTOS_H

typedef struct{
    int id;
    char nome[200];
    char data[20]; // 22 03 2019
    int idade;
    int peso;
    float experiencia;
    int impedimento; // numero de dias de impedimento
}Piloto;

Piloto* carrega_pilotos(char *filename, int *size );
void escreve_pilotos(Piloto * lista, int size);
int penalizar_piloto(int id, int penalizacao, Piloto * lista, int size);
int calcula_idade(int hdia, int hmes, int hano, int dia, int mes, int ano);
void guarda_pilotos(char * filename, Piloto * lista_pilotos, int size_pilotos);
int verifica_piloto(Piloto * lista, int size, int id);

#endif //PROJETO_PILOTOS_H
