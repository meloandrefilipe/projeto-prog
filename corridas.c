/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */


#include "carros.h"
#include "pilotos.h"
#include <stdio.h>
#include <stdlib.h>
#include "corridas.h"
#include <string.h>
#include <time.h>
#include "utils.h"
#include <string.h>


Corrida * criar_corrida(int voltas, int cumprimento, int capacidade, Piloto * lista_pilotos, Carro * lista_carros,int size_pilotos,int size_carros){
    
    Corrida * corrida;
    Carro * carros_corrida;
    Carro * carros_fora;
    Piloto * pilotos_corrida;
    Piloto * pilotos_fora;
    int size = 0;
    
    corrida = (Corrida *)malloc(sizeof(Corrida));
    carros_corrida = (Carro *)malloc(sizeof(Carro));
    carros_fora = (Carro *)malloc(sizeof(Carro));
    pilotos_corrida = (Piloto *)malloc(sizeof(Piloto));
    pilotos_fora = (Piloto *)malloc(sizeof(Piloto));
    
    int carros_adicionados = 0, pilotos_adicionados = 0 , carros_f = 0, pilotos_f = 0; 
    for(int i  = 0; i < size_carros; i++){
        if((lista_carros[i].avariado == 0)){
            if(carros_adicionados > 0){
                carros_corrida = realloc(carros_corrida, sizeof(Carro) * (carros_adicionados +1));
            }
            memcpy(&carros_corrida[carros_adicionados], &lista_carros[i], sizeof(Carro));
            carros_adicionados++;
        }else{
            if(carros_f > 0){
                carros_fora = realloc(carros_fora, sizeof(Carro) * (carros_f +1));
            }
            memcpy(&carros_fora[carros_f], &lista_carros[i], sizeof(Carro));
            carros_f++;
        }
    }
    for(int i  = 0; i < size_pilotos; i++){
        if((lista_pilotos[i].impedimento == 0)){
            if(pilotos_adicionados > 0){
                pilotos_corrida = realloc(pilotos_corrida, sizeof(Piloto) * (pilotos_adicionados +1));
            }
            memcpy(&pilotos_corrida[pilotos_adicionados], &lista_pilotos[i], sizeof(Piloto));
            pilotos_adicionados++;
        }else{
            if(pilotos_f > 0){
                pilotos_fora = realloc(pilotos_fora, sizeof(Piloto) * (pilotos_f +1));
            }
            memcpy(&pilotos_fora[pilotos_f], &lista_pilotos[i], sizeof(Piloto));
            pilotos_f++;
        }
    }
    if((capacidade > carros_adicionados) && (pilotos_adicionados >= carros_adicionados)){
        size = carros_adicionados;
    }else if ((capacidade > pilotos_adicionados) && (carros_adicionados >= pilotos_adicionados) ){
        size = pilotos_adicionados;
    }else{
        size = capacidade;
    }
    corrida->voltas = voltas;
    corrida->cumprimento = cumprimento;
    corrida->capacidade = capacidade;
    corrida->pilotos_prova = pilotos_adicionados;
    corrida->carros_prova = carros_adicionados;
    corrida->pilotos_penalizados = pilotos_f;
    corrida->carros_penalizados = carros_f;
    corrida->carros = carros_corrida;
    corrida->carros_fora = carros_fora;
    corrida->pilotos = pilotos_corrida;
    corrida->pilotos_fora = pilotos_fora;
    corrida->tamanho = size;
    corrida->resultados = NULL;
    return corrida;
}
Corrida * inicia_corrida(Corrida * corrida){
    srand(time(NULL));
    Resultados  *resultado, *aux, *inicio;
    
    int data[corrida->tamanho][2];
    
    for (int i = 0; i < corrida->tamanho; i++){
        data[i][0] = data[i][1] = -1;
    }
    for (int i = 0; i < corrida->tamanho; i++){
        data[i][0] = carro_aleatorio(corrida,data,corrida->tamanho);
        data[i][1] = piloto_aleatorio(corrida,data,corrida->tamanho);
    }
    escreve_selecao(corrida, data);
    for (int i = 0; i < corrida->tamanho; i++){
        resultado = malloc(sizeof(Resultados));
        resultado->carro = (Carro *)malloc(sizeof(Carro));
        resultado->piloto = (Piloto *)malloc(sizeof(Piloto));
        for(int p = 0; p < corrida->pilotos_prova; p++){
            if (corrida->pilotos[p].id == data[i][1]){
                resultado->piloto = &corrida->pilotos[p];
            }
        }
        for(int c = 0; c < corrida->carros_prova; c++){
            if (corrida->carros[c].id == data[i][0]){
                resultado->carro = &corrida->carros[c];
            }
        }
        pVolta volta, vaux, inicial;
        resultado->tempo_total = 0;
        resultado->acidente = 0;
        int total_completas = 0;
        for (int v = 0; v < corrida->voltas; v++){
            volta = malloc(sizeof(Voltas));
            if(resultado->acidente == 0){
                volta->tempo = calculaSegundos(resultado->piloto->idade,resultado->piloto->peso,resultado->piloto->experiencia,resultado->carro->potencia,corrida->cumprimento);
                volta->volta = v + 1;
                volta->proxima = NULL;
                if(probEvento(0.05) > 0){
                    resultado->acidente = total_completas +1;
                    for(int p = 0; p < corrida->pilotos_prova; p++){
                        if(corrida->pilotos[p].id == resultado->piloto->id){
                            corrida->pilotos[p].impedimento += 2;
                        }
                    }
                    for(int c = 0; c < corrida->carros_prova; c++){
                        if(corrida->carros[c].id == resultado->carro->id){
                            corrida->carros[c].avariado += 1;
                        }
                    }
                }
                resultado->tempo_total += volta->tempo;
                total_completas++;
            }else{
                volta->tempo = 0;
                volta->volta = v+1;
            }
            if(v != 0){
                vaux->proxima = volta;
                vaux = volta;
            }else{
                inicial = volta;
                vaux = volta;
            }
        }
        resultado->voltas = inicial;
        resultado->proximo = NULL;
        if(i == 0){
            inicio = resultado;
        }else{
            aux->proximo = resultado;
        }
        aux = resultado;
    }
    corrida->resultados = inicio;
    return escreve_resultados_atualiza(corrida);
}


int carro_aleatorio(Corrida * corrida, int data[][2], int size){
    int check = 1;
    int id;
    while(check == 1){
        check = 0;
        id = corrida->carros[rand()%corrida->carros_prova].id;
        for (int i = 0; i < size; i++){
            if(id == data[i][0]){
                check = 1;
            }
        }
    }
    return id;
}
int piloto_aleatorio(Corrida * corrida, int data[][2], int size){
    int check = 1;
    int id;
    while(check == 1){
        check = 0;
        id = corrida->pilotos[rand()%corrida->pilotos_prova].id;
        for (int i = 0; i < size; i++){
            if(id == data[i][1]){
                check = 1;
            }
        }
    }
    return id;
}

void atualizar_penalizacoes(Piloto * pilotos, Carro * carros, int size_pilotos,int size_carros ){
    for(int i = 0; i < size_pilotos; i++){
        if(pilotos[i].impedimento > 0){
            pilotos[i].impedimento--;
        }
    }
    for(int i = 0; i < size_carros; i++){
        if(carros[i].avariado > 0){
            carros[i].avariado--;
        }
    }
}

void escreve_resultados(pResultado resultados, int voltas){
    int tempo;
    int pos = 1;
    pResultado temp;
    for (int j = 0; j < voltas;j++){
        pos = 1;
        if(j == voltas -1){
            printf("\nClassificacao final\n");
        }else{
            printf("\nClassificacao ao final da %da volta\n",j+1);
        }
        ordenar_resultados(resultados,j+1);
        for (temp = resultados; temp != NULL; temp = temp->proximo){
            tempo = 0;
            int crash = 0;
            if(j+1 >= temp->acidente && temp->acidente != 0){
                printf("[Acidente Volta %d] %s (id: %d) / Carro %d: ",temp->acidente,temp->piloto->nome,temp->piloto->id, temp->carro->id);
            }else{
                printf("[%d] %s (id: %d) / Carro %d: ",pos,temp->piloto->nome,temp->piloto->id, temp->carro->id);
            }
            
            pVolta volta = temp->voltas;
            for(int v = 0; v <= j; v++){
                if((temp->acidente != (v + 1) ) && crash == 0){
                    if(v == 0){
                        printf("%d",volta->tempo);
                    }else{
                        printf(" + %d",volta->tempo);  
                    }
                    tempo += volta->tempo;
                }else{
                    if(crash == 0){
                        if(temp->acidente == 1){
                            printf(" %d segundos",tempo);
                        }else{
                            printf(" : %d segundos",tempo);
                        }
                        printf(" Desqualificado\n");
                        crash++;
                    }
                }
                volta = volta->proxima;
            }
            if(crash == 0){
                printf(" : %d segundos\n",tempo);
            }
            pos++;
        }
    }
}
Corrida * escreve_resultados_atualiza(Corrida * corrida){
    int tempo;
    int pos = 1;
    int voltas = corrida->voltas;
    pResultado resultados = corrida->resultados;
    pResultado temp;
    for (int j = 0; j < voltas;j++){
        pos = 1;
        espera(5);
        if(j == voltas -1){
            printf("\nClassificacao ao final\n");
        }else{
            printf("\nClassificacao ao final da %da volta\n",j+1);
        }
        
        ordenar_resultados(resultados,j+1);
        for (temp = resultados; temp != NULL; temp = temp->proximo){
            tempo = 0;
            int crash = 0;
            if(j+1 >= temp->acidente && temp->acidente != 0){
                printf("[Acidente Volta %d] %s (id: %d) / Carro %d: ",temp->acidente,temp->piloto->nome,temp->piloto->id, temp->carro->id);
            }else{
                printf("[%d] %s (id: %d) / Carro %d: ",pos,temp->piloto->nome,temp->piloto->id, temp->carro->id);
            }
            if(pos == 1){
                for(int p = 0; p < corrida->pilotos_prova; p++){
                    if(corrida->pilotos[p].id == temp->piloto->id){
                        corrida->pilotos[p].experiencia +=0.5;
                    }
                }
            }
            pVolta volta = temp->voltas;
            for(int v = 0; v <= j; v++){
                if((temp->acidente != (v + 1) ) && crash == 0){
                    if(v == 0){
                        printf("%d",volta->tempo);
                    }else{
                        printf(" + %d",volta->tempo);  
                    }
                    tempo += volta->tempo;
                }else{
                    if(crash == 0){
                        if(temp->acidente == 1){
                            printf(" %d segundos",tempo);
                        }else{
                            printf(" : %d segundos",tempo);
                        }
                        printf(" Desqualificado\n");
                        crash++;
                    }
                }
                volta = volta->proxima;
                if(v == voltas -1 && temp->acidente != 0){
                    for(int p = 0; p < corrida->pilotos_prova; p++){
                        if(corrida->pilotos[p].id == temp->piloto->id){
                            if(corrida->pilotos[p].experiencia >= 1){
                               corrida->pilotos[p].experiencia -=1;
                            }else{
                               corrida->pilotos[p].experiencia = 0;
                            }
                        }
                    }
                }
            }
            if(crash == 0){
                printf(" : %d segundos\n",tempo);
            }
            pos++;
        }
    }
    return corrida;
}
void escreve_resultados_volta(pResultado resultados, int volta){
    int pos = 1;
    pResultado temp;
    printf("\nClassificacao no final da volta #%d\n",volta);
    ordenar_resultados(resultados,volta);
    for (temp = resultados; temp != NULL; temp = temp->proximo){
        if(volta >= temp->acidente && temp->acidente != 0){
            printf("[Acidente Volta %d] %s (id: %d) / Carro %d: ",temp->acidente,temp->piloto->nome,temp->piloto->id, temp->carro->id);
        }else{
            printf("[%d] %s (id: %d) / Carro %d: ",pos,temp->piloto->nome,temp->piloto->id, temp->carro->id);
        }
        pVolta voltas = temp->voltas;
        while(voltas != NULL){
            voltas = voltas->proxima;
            if(voltas->volta == volta){
                break;
            }
        }
        if(voltas != NULL){
            if((temp->acidente != volta )){
                printf("Tempo: %d\n",voltas->tempo);
            }else{
                printf("[Acidente] - Piloto Desqualificado!\n");
            }
        }else{
            printf("[ERRO] Volta nao existe!\n");
            return;
        }
        pos++;
    }
}

void ordenar_resultados(pResultado resultado, int volta){
    pResultado a = resultado;
    if(a == NULL){
        return;
    }
    while(a != NULL){
        for(pResultado b = a->proximo; b != NULL; b = b->proximo){
            if(verifica_troca(a,b,volta)){
                troca_valores(a,b);
            }
        }
        a = a->proximo;
    }
}

int verifica_troca(pResultado a, pResultado b, int volta){
    int total_lista = 0;
    int total_resto = 0;
    pVolta lista_volta = a->voltas;
    for(int i = 1; i <= volta; i++){
        if(a->acidente == 0){
            total_lista += lista_volta->tempo;
        }else{
            total_lista= 999999;
        }
        lista_volta = lista_volta->proxima;
    }
    pVolta resto_volta = b->voltas;
    for(int i = 1; i <= volta; i++){
        if(b->acidente == 0){
            total_resto += resto_volta->tempo;
        }else{
            total_resto = 999999;
        }
        resto_volta = resto_volta->proxima;
    }
    if(total_resto < total_lista){
        return 1;
    }
    return 0;
}
void troca_valores(pResultado a, pResultado b){
    Piloto *p = b->piloto;
    Carro *c = b->carro;
    pVolta v = b->voltas;
    int acidente = b->acidente;
    int total = b->tempo_total;
    b->piloto = a->piloto;
    a->piloto = p;
    b->carro = a->carro;
    a->carro = c;
    b->voltas = a->voltas;
    a->voltas = v;
    b->acidente = a->acidente;
    a->acidente = acidente;
    b->tempo_total = a->tempo_total;
    a->tempo_total = total;
}
void atualiza_listas(Corrida * corrida, Piloto * pilotos, Carro * carros, int size_pilotos, int size_carros){
    for(int p = 0; p < size_pilotos; p++){
        for(int c = 0; c < corrida->pilotos_prova; c++){
            if(corrida->pilotos[c].id == pilotos[p].id){
                pilotos[p].experiencia = corrida->pilotos[c].experiencia;
                pilotos[p].impedimento = corrida->pilotos[c].impedimento;
            }
        }
    }
    for (int c = 0; c < size_carros; c++){
        for (int i = 0; i < corrida->carros_prova; i++){
            if(carros[c].id == corrida->carros[i].id){
                carros[c].avariado = corrida->carros[i].avariado;
            }
        }
    }
}
void escreve_selecao(Corrida * corrida, int emparelhamento[][2]){
    printf("Emparelhamento da corrida:\n");
    for(int i = 0; i < corrida->tamanho; i++){
        for (int p = 0; p < corrida->pilotos_prova; p++){
            if(corrida->pilotos[p].id == emparelhamento[i][1]){
                printf("\t- Piloto %s com o carro #%d\n",corrida->pilotos[p].nome,emparelhamento[i][0]);
            }
        }
    }
    int carros_fora = 0;
    printf("Carros fora:\n");
    for (int c = 0; c < corrida->carros_penalizados; c++){
        if(corrida->carros_fora[c].avariado != 0){
            printf("\t- Carro #%d esta avariado!\n",corrida->carros_fora[c].id);
        }
    }
    for(int c = 0; c < corrida->carros_prova; c++){
        int existe = 0;
        for(int e = 0; e < corrida->tamanho; e++){
            if(emparelhamento[e][0] == corrida->carros[c].id){
                existe = 1;
            }
        }
        if(existe == 0){
            printf("\t- Carro #%d nao tem piloto!\n",corrida->carros[c].id);
            carros_fora++;
        }
    }
    printf("Pilotos fora:\n");
    for (int p = 0; p < corrida->pilotos_penalizados; p++){
        if(corrida->pilotos_fora[p].impedimento != 0){
            printf("\t- O piloto %s tem uma penalizacao de %d corrida/s!\n",corrida->pilotos_fora[p].nome,corrida->pilotos_fora[p].impedimento);
        }else{
            printf("\t- O piloto %s nao tem um carro!\n",corrida->pilotos_fora[p].nome);
        }
    }
    for (int p =0 ; p < corrida->pilotos_prova; p++){
        int existe = 0;
        for(int e = 0; e < corrida->tamanho; e++){
            if(emparelhamento[e][1] == corrida->pilotos[p].id){
                existe = 1;
            }
        }
        if(existe == 0){
            if(carros_fora == 0){
                printf("\t- O piloto %s nao tem um carro!\n",corrida->pilotos[p].nome);
            }else{
                printf("\t- O piloto %s nao tem vaga na corrida!\n",corrida->pilotos[p].nome);
            }
        }
    }
    
}