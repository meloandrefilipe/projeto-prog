/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */

#include "carros.h"
#include "pilotos.h"
#include <stdio.h>
#include <stdlib.h>
#include "corridas.h"
#include <string.h>
#include <time.h>
#include "utils.h"
#include <string.h>
#include "campeonato.h"


pCampeonato criar_campeonato(pCampeonato campeonato ,int total_provas){
    
    campeonato = malloc(sizeof(Campeonato));
    campeonato->total_provas = total_provas;
    campeonato->podio = NULL;
    campeonato->provas = NULL;
    campeonato->provas_decorridas = 0;
    campeonato->tam_podio = tamanho_podio(campeonato->podio);
    return campeonato;
}
pCampeonato avanca_campeonato(pCampeonato campeonato,int capacidade, int distancia, int voltas ,Piloto * lista_pilotos, Carro * lista_carros,int size_pilotos,int size_carros){
    pProva prova = malloc(sizeof(Prova));
    pPodio podio = campeonato->podio;
    prova->corrida = criar_corrida(voltas,distancia,capacidade,lista_pilotos,lista_carros,size_pilotos,size_carros);
    prova->corrida = inicia_corrida(prova->corrida);
    prova->proxima = NULL;
    if(campeonato->provas_decorridas == 0){
        campeonato->provas = prova;
    }else{
        pProva aux = campeonato->provas;
        while(aux->proxima != NULL){
            aux = aux->proxima;
        }
        aux->proxima = prova;
    }
    podio = atualiza_podio(prova->corrida,podio);
    campeonato->podio = podio;
    campeonato->provas_decorridas++;
    if(campeonato->provas_decorridas == campeonato->total_provas){
        campeonato->podio->piloto->experiencia += 10; 
        printf("O piloto %s e o CAMPEAO com %d pontos!\n",campeonato->podio->piloto->nome,campeonato->podio->pontos);
    }
    campeonato->tam_podio = tamanho_podio(podio);
    atualizar_penalizacoes(lista_pilotos,lista_carros,size_pilotos,size_carros);
    atualiza_listas(prova->corrida,lista_pilotos,lista_carros,size_pilotos,size_carros);
}
int guardar_campeonato(pCampeonato campeonato, char * filename){
    FILE *file = fopen(filename,"wb");

    if(file == NULL){
        printf("Erro ao aceder ao ficheiro %s\n",filename);
        exit(1);
    }
    fwrite(&campeonato->total_provas, sizeof(int), 1, file);
    fwrite(&campeonato->provas_decorridas, sizeof(int), 1, file);
    fwrite(&campeonato->tam_podio, sizeof(int), 1, file);
    pProva provas = campeonato->provas;
    while(provas != NULL){
        fwrite(&provas->corrida->voltas, sizeof(int), 1, file);
        fwrite(&provas->corrida->cumprimento, sizeof(int), 1, file);
        fwrite(&provas->corrida->capacidade, sizeof(int), 1, file);
        fwrite(&provas->corrida->pilotos_prova, sizeof(int), 1, file);
        fwrite(&provas->corrida->carros_prova, sizeof(int), 1, file);
        fwrite(&provas->corrida->pilotos_penalizados, sizeof(int), 1, file);
        fwrite(&provas->corrida->carros_penalizados, sizeof(int), 1, file);
        fwrite(&provas->corrida->tamanho, sizeof(int), 1, file);
        for(int c = 0; c < provas->corrida->carros_prova; c++){
            fwrite(&provas->corrida->carros[c], sizeof(Carro),1, file);
        }
        for(int p = 0; p < provas->corrida->pilotos_prova; p++){
            fwrite(&provas->corrida->pilotos[p], sizeof(Piloto),1, file);
        }
        for(int c = 0; c < provas->corrida->carros_penalizados; c++){
            fwrite(&provas->corrida->carros_fora[c], sizeof(Carro),1, file);
        }
        for(int p = 0; p < provas->corrida->pilotos_penalizados; p++){
            fwrite(&provas->corrida->pilotos_fora[p], sizeof(Piloto),1, file);
        }
        pResultado resultados = provas->corrida->resultados;
        while(resultados != NULL){
            fwrite(&resultados->carro[0], sizeof(Carro),1, file);
            fwrite(&resultados->piloto[0], sizeof(Piloto),1, file);
            
            pVolta voltas = resultados->voltas;
            while(voltas != NULL){
                fwrite(&voltas->volta, sizeof(int), 1, file);
                fwrite(&voltas->tempo, sizeof(int), 1, file);
                voltas = voltas->proxima;
            }
            fwrite(&resultados->acidente, sizeof(int), 1, file);
            fwrite(&resultados->tempo_total, sizeof(int), 1, file);
            resultados = resultados->proximo;
        }
        provas = provas->proxima;
    }   
    pPodio podio = campeonato->podio;
    while(podio != NULL){
        fwrite(&podio->acidente, sizeof(int), 1, file);
        fwrite(&podio->pontos, sizeof(int), 1, file);
        fwrite(&podio->corridas_participadas, sizeof(int), 1, file);
        fwrite(&podio->piloto[0], sizeof(Piloto),1, file);
        podio = podio->proximo;
    }
    fclose(file);
    return 1;
}
pCampeonato carregar_campeonato(pCampeonato campeonato, char * filename){
    FILE *file = fopen(filename,"rb");
    if(file == NULL){
        return 0;
    }
        
    if(campeonato == NULL){
        campeonato = malloc(sizeof(Campeonato));
        fread(&campeonato->total_provas, sizeof(int), 1, file);
        fread(&campeonato->provas_decorridas, sizeof(int), 1, file);
        fread(&campeonato->tam_podio, sizeof(int), 1, file);
        pProva anterior, inicio;
        for(int i = 0; i < campeonato->provas_decorridas; i++){
            pProva provas = malloc(sizeof(Prova));
            Corrida *corrida = malloc(sizeof(Corrida));
            fread(&corrida->voltas, sizeof(int), 1, file);
            fread(&corrida->cumprimento, sizeof(int), 1, file);
            fread(&corrida->capacidade, sizeof(int), 1, file);
            fread(&corrida->pilotos_prova, sizeof(int), 1, file);
            fread(&corrida->carros_prova, sizeof(int), 1, file);
            fread(&corrida->pilotos_penalizados, sizeof(int), 1, file);
            fread(&corrida->carros_penalizados, sizeof(int), 1, file);
            fread(&corrida->tamanho, sizeof(int), 1, file);

            
            Carro * carros = (Carro *)malloc(sizeof(Carro) * corrida->carros_prova);
            for(int c = 0; c < corrida->carros_prova; c++){
                fread(&carros[c], sizeof(Carro),1, file);
            }
            corrida->carros = carros;
            Piloto * pilotos= (Piloto *)malloc(sizeof(Piloto) *corrida->pilotos_prova);
            for(int p = 0; p < corrida->pilotos_prova; p++){
                fread(&pilotos[p], sizeof(Piloto),1, file);
            }
            corrida->pilotos = pilotos;
            Carro * carros_fora = (Carro *)malloc(sizeof(Carro) * corrida->carros_penalizados);
            for(int c = 0; c < corrida->carros_penalizados; c++){
                fread(&carros_fora[c], sizeof(Carro),1, file);
            }
            corrida->carros_fora = carros_fora;
            Piloto * pilotos_fora = (Piloto *)malloc(sizeof(Piloto) * corrida->pilotos_penalizados);
            for(int p = 0; p < corrida->pilotos_penalizados; p++){
                fread(&pilotos_fora[p], sizeof(Piloto),1, file);
            }
            corrida->pilotos_fora = pilotos_fora;

            
            pResultado rAnterior, rInicio;
            for(int r = 0; r < corrida->tamanho; r++){
                pResultado resultados = malloc(sizeof(Resultados));
                Carro * carro = (Carro *)malloc(sizeof(Carro));
                Piloto * piloto = (Piloto *)malloc(sizeof(Piloto));
                fread(&carro[0], sizeof(Carro),1, file);
                fread(&piloto[0], sizeof(Piloto),1, file);
                resultados->carro = &carro[0];
                resultados->piloto = &piloto[0];
                pVolta vAnterior, vInicio;
                for(int v = 0; v < corrida->voltas; v++){
                    pVolta voltas = malloc(sizeof(Voltas));
                    fread(&voltas->volta, sizeof(int), 1, file);
                    fread(&voltas->tempo, sizeof(int), 1, file);
                    voltas->proxima = NULL;
                    if(v > 0){
                        vAnterior->proxima = voltas;
                    }
                    if (v == 0){
                        vInicio = voltas;
                    }
                    vAnterior = voltas;
                }
                resultados->voltas = vInicio;
                fread(&resultados->acidente, sizeof(int), 1, file);
                fread(&resultados->tempo_total, sizeof(int), 1, file);
                resultados->proximo = NULL;
                if(r > 0){
                    rAnterior->proximo = resultados;
                }
                if(r == 0){
                    rInicio = resultados;
                }
                rAnterior = resultados;
            }
            corrida->resultados = rInicio;
            provas->corrida = corrida;
            provas->proxima = NULL;
            if(i > 0){
                anterior->proxima = provas;
            }
            if(i == 0){
                inicio = provas;
            }
            anterior = provas;
        }
        pPodio pInicio,pAnterior;
        for(int p = 0; p < campeonato->tam_podio; p++){
            pPodio podio = malloc(sizeof(Podio));
            Piloto * podio_piloto = (Piloto *)malloc(sizeof(Piloto));
            fread(&podio->acidente, sizeof(int), 1, file);
            fread(&podio->pontos, sizeof(int), 1, file);
            fread(&podio->corridas_participadas, sizeof(int), 1, file);
            fread(&podio_piloto[0], sizeof(Piloto),1, file);
            podio->piloto = &podio_piloto[0];
            podio->proximo = NULL;
            if(p > 0){
                pAnterior->proximo = podio;
            }
            if(p == 0){
                pInicio = podio;
            }
            pAnterior = podio;
        }
        campeonato->podio = pInicio;    
        campeonato->provas = inicio;
        fclose(file);
    }
    return campeonato;
}

pPodio atualiza_podio(pCorrida corrida, pPodio podio){
    pPodio aux, inicio, anterior;
    aux = podio;
    pResultado resultados = corrida->resultados;
    ordenar_resultados(resultados, corrida->voltas);
    int pontos = 5;
    int inicial = 1;
    if(aux == NULL){
        for(pResultado temp = resultados; temp != NULL; temp = temp->proximo){
            aux = malloc(sizeof(Podio));
            aux->pontos = 0;
            aux->corridas_participadas = 1;
            if(temp->acidente == 0){
                aux->acidente = 0;
//                if(pos == 1){
//                    for(int p = 0; p < corrida->pilotos_prova; p++){
//                        if(corrida->pilotos[p].id == temp->piloto->id){
//                            corrida->pilotos[p].experiencia +=0.5;
//                        }
//                    }
//                }
                if(pontos > 0){
                    aux->pontos = pontos;
                    pontos--;
                }
            }else{
                aux->acidente = 1;
            }
            aux->piloto = temp->piloto;
            aux->proximo = NULL;
            if(inicial == 1){
                inicio = aux;
            }else{
                anterior->proximo = aux;
            }
            anterior = aux;
            inicial++;
        }
        return inicio;
    }else{
        for(pResultado temp = resultados; temp != NULL; temp = temp->proximo){
            if(no_podio(podio,temp->piloto)){
                for(aux = podio; aux != NULL; aux = aux->proximo){
                    if(aux->piloto->id == temp->piloto->id){
                        if(temp->acidente == 0){
    //                        if(pos == 1){
    //                            for(int p = 0; p < corrida->pilotos_prova; p++){
    //                                if(corrida->pilotos[p].id == temp->piloto->id){
    //                                    corrida->pilotos[p].experiencia +=0.5;
    //                                }
    //                            }
    //                        }
                            if(pontos > 0){
                                aux->pontos += pontos;
                                pontos--;
                            }
                        }else{
                            aux->acidente++;
                        }
                        aux->corridas_participadas++;
                    }
                }
            }else{
                pPodio anterior = podio;
                while(anterior->proximo != NULL){
                    anterior = anterior->proximo;
                }
                pPodio novo = malloc(sizeof(Podio));
                novo->acidente = temp->acidente;
                novo->corridas_participadas = 1;
                novo->piloto = temp->piloto;
                novo->proximo = NULL;
                if(temp->acidente == 0){
                    novo->pontos = pontos; 
                }else{
                    novo->pontos = 0;
                }
                anterior->proximo = novo;
                pontos--;
            }
        }
        return podio;
    }
}
void escreve_podio(pPodio podio){
    ordena_podio(podio);
    pPodio aux = podio;
    int pos = 1;
    if(aux == NULL){
        printf("\nPodio vazio!\n");
        return;
    }
    printf("\nPodio da Corrida\n");
    while(aux != NULL){
        printf("\t[%d] - %s [%d pontos em %d corridas]\n",pos,aux->piloto->nome,aux->pontos, aux->corridas_participadas);
        pos++;
        aux = aux->proximo;
    }
}

void ordena_podio(pPodio podio){
    pPodio a = podio;
    if(a == NULL){
        return;
    }
    while(a != NULL){
        for(pPodio b = a->proximo; b != NULL; b = b->proximo){
            if(verifica_podio(a,b)){
                troca_podio(a,b);
            }
        }
    a = a->proximo;
    }
}
int verifica_podio(pPodio a, pPodio b){
    if(a->pontos < b->pontos){
        return 1;
    }else if(a->pontos == b->pontos && a->corridas_participadas > b->corridas_participadas){
        return 1;
    }else if(a->pontos == b->pontos && a->corridas_participadas == b->corridas_participadas && a->piloto->idade > b->piloto->idade){
        return 1;
    }
    return 0;
}
void troca_podio(pPodio a,pPodio b){
    int acidente = a->acidente;
    int pontos = a->pontos;
    int corridas = a->corridas_participadas;
    Piloto * piloto = a->piloto;
    a->acidente = b->acidente;
    a->pontos = b->pontos;
    a->corridas_participadas = b->corridas_participadas;
    a->piloto = b->piloto;
    b->acidente = acidente;
    b->pontos = pontos;
    b->corridas_participadas = corridas;
    b->piloto = piloto;
}
int no_podio(pPodio podio, Piloto * piloto){
    pPodio aux = podio;
    while(aux != NULL){
        if(aux->piloto->id == piloto->id){
            return 1;
        }
        aux = aux->proximo;
    }
    return 0;
}
int tamanho_podio(pPodio podio){
    pPodio aux = podio;
    if(aux == NULL){
        return 0;
    }
    int tam = 0;
    while(aux != NULL){
        tam++;
        aux = aux->proximo;
    }
    return tam;
}