/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "pilotos.h"


Piloto* carrega_pilotos(char *filename, int *size_pilotos ){
    
    FILE *file = fopen(filename,"rt");
    Piloto *lista;
    char  nome[200];
    int id, peso, impedimento, dia, mes, ano, size = 0;
    float experiencia;
    int hdia, hmes, hano, hmin,hhora, hsec;
    obtemData(&hdia,&hmes,&hano,&hhora,&hmin,&hsec); // esta funçao tem um erro no mes, devolve um mes a menos que o atual
    if(file == NULL){
        printf("Erro ao aceder ao ficheiro %s\n",filename);
        return NULL;
    }
    lista = (Piloto *)malloc(sizeof(Piloto));
    do{
        int read_nome = fscanf(file, " %[^\n]",nome);
        if(read_nome != EOF){
            int read = fscanf(file, "%d %d %d %d %d %f %d",&id, &dia, &mes, &ano,&peso, &experiencia, &impedimento);
            if(read != EOF){
                if(read == 7 && !verifica_piloto(lista,size,id)){
                    sprintf(lista[size].data,"%d %d %d",dia,mes,ano);
                    lista[size].idade = calcula_idade(hdia, hmes, hano, dia, mes, ano); //a idade sai mal devido ao erro na função obtemData do utils.h
                    lista[size].experiencia = experiencia;
                    lista[size].impedimento = impedimento;
                    lista[size].id = id;
                    lista[size].peso = peso;
                    strcpy(lista[size].nome, nome);
                    size++;
                    lista = realloc(lista, sizeof(Piloto) * (size +1));
                }else{
                    free(lista);
                    return NULL;
                }
            }
        }
    }while(feof(file) == 0);
    fclose(file);
    *size_pilotos = size;
    return lista;
}
int verifica_piloto(Piloto * lista, int size, int id){
    for(int i = 0; i < size; i++){
        if(lista[i].id == id){
            return 1;
        }
    }
    return 0;
}
void escreve_pilotos(Piloto * lista, int size){
    for(int i = 0 ; i < size; i++){
        printf("Nome: %s\n",lista[i].nome);
        printf("\n");
        printf(" - ID: %d\n",lista[i].id);
        printf(" - Data de nascimento: %s\n",lista[i].data);
        printf(" - Idade: %d\n",lista[i].idade);
        printf(" - Peso: %d\n",lista[i].peso);
        printf(" - Experiencia: %0.6g\n",lista[i].experiencia);
        printf(" - Dias de penalizacao: %d\n",lista[i].impedimento);
        printf("\n");
    }
}

int penalizar_piloto(int id, int penalizacao, Piloto * lista, int size){
    for(int i = 0; i < size; i++){
        if(lista[i].id == id){
            lista[i].impedimento += penalizacao ;
            return i;
        }
    }
    return -1 ;
}
int calcula_idade(int hdia, int hmes, int hano, int dia, int mes, int ano){
    int idade = hano - ano;
    if(hmes < mes){
        idade--;
    }
    if(hmes == mes){
        if (hdia < dia){
            idade--;
        }
    }
    return idade;
}
void guarda_pilotos(char * filename, Piloto * lista_pilotos, int size_pilotos){
    FILE *file = fopen(filename,"wt");
    if(file == NULL){
        printf("Erro ao aceder ao ficheiro %s\n",filename);
        return;
    }
    for (int p = 0; p < size_pilotos; p++){
           fprintf(file, "%s\n\0",lista_pilotos[p].nome);
           fprintf(file, "%d %s %d %.1f %d\n\n\0",lista_pilotos[p].id,lista_pilotos[p].data,lista_pilotos[p].peso,lista_pilotos[p].experiencia,lista_pilotos[p].impedimento);
    }
    fclose(file);
}