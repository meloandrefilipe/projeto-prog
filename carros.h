/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */


#ifndef CARROS_H
#define CARROS_H

typedef struct{
    int id;
    int potencia;
    int avariado;
}Carro;

Carro* carrega_carros(char *filename, int *size_carros );
void escreve_carros(Carro * lista, int size);
int penalizar_carro(int id, Carro * lista, int size);
void guarda_carros(char * filename, Carro * lista_carros, int size_carros);
int verifica_carro(Carro * lista, int size, int id);
#endif /* CARROS_H */

