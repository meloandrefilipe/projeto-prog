/* 
 * Autor: André Filipe Silva Melo
 * Numero: 21270968
 * Unidade Curricular: Programação
 * Projeto: Karts@ISEC
 * Ano Curricular: 2018/2019
 */


#include <stdio.h>
#include <stdlib.h>
#include "carros.h"


Carro* carrega_carros(char *filename, int *size_carros ){
    
    FILE *file = fopen(filename,"rt");
    Carro * lista;
    int id, potencia, avariado, size = 0;
   
    if(file == NULL){
        printf("Erro ao aceder ao ficheiro %s\n",filename);
        return NULL;
    }
    lista = (Carro *)malloc(sizeof(Carro));
    do{
        int read = fscanf(file, "%d %d %d",&id,&potencia, &avariado);
        if (read != EOF){
            if(read == 3 && !verifica_carro(lista,size,id)){
                lista[size].avariado = avariado;
                lista[size].id = id;
                lista[size].potencia = potencia;
                size++;
                lista = realloc(lista, sizeof(Carro) * (size +1));
            }else{
                free(lista);
                return NULL;
            }
        }
    }while(feof(file) == 0);
    fclose(file);
    *size_carros = size;
    return lista;
}
int verifica_carro(Carro * lista, int size, int id){
    for(int i = 0; i < size; i++){
        if(lista[i].id == id){
            return 1;
        }
    }
    return 0;
}
void escreve_carros(Carro * lista, int size){
    for(int i = 0 ; i < size; i++){
        printf("Carro n%d:\n",lista[i].id);
        printf("\n");
        printf(" - Potencia: %d\n",lista[i].potencia);
        printf(" - Avaria (dias): %d\n",lista[i].avariado);
        printf("\n");
    }
}
int penalizar_carro(int id, Carro * lista, int size){
    for(int i = 0; i < size; i++){
        if(lista[i].id == id){
            lista[i].avariado += 1;
            return i;
        }
    }
    return -1 ;
}
void guarda_carros(char * filename,Carro * lista_carros, int size_carros){
    FILE *file = fopen(filename,"wt");
    if(file == NULL){
        printf("Erro ao aceder ao ficheiro %s\n",filename);
        return;
    }
    for (int p = 0; p < size_carros; p++){
           fprintf(file, "%d %d %d\n\n\0",lista_carros[p].id,lista_carros[p].potencia,lista_carros[p].avariado);
    }
    fclose(file);
}